<?php 
/**
 * 
 * 
 */

add_action('init', 'bcmh_image_sizes');

function bcmh_image_sizes() {

	update_option('medium_size_w', 680);
	update_option('medium_size_h', 680);


    add_image_size( 'palm', 400, 350, false );
    add_image_size( 'tablet', 800, 9999, false );
    add_image_size( 'desk', 1400, 9999, false );
    add_image_size( 'plus', 2400, 9999, false );
}

function bcmh_enqueue_picturefill() {

 // Check files exist
 if ( !file_exists( get_template_directory() . '/assets/javascript/libs/picturefill.js') ) {

 	throw new Exception("Error Loading Dependency Picturefill", 1);
 	exit();
 }
 wp_enqueue_script( 'picturefill', get_template_directory_uri() . '/assets/javascript/libs/picturefill.js', false, 0, true );
}

add_action( 'wp_enqueue_scripts', 'bcmh_enqueue_picturefill' );


// Sort our image sizes by width
function orderImagesByWidth($a, $b) {
    if ( $a['width'] == $b['width'] ) {
        return 0;
    }

    return ($a['width'] < $b['width']) ? -1 : 1;
}

function get_image_ratio( $width, $height ) {
	return $width > $height ? $width / $height : $height / $width;
}

function bcmh_picture_element( $image_id ) { 

		$sizes_as_ems = array(
			'medium'	 => '',
			'tablet' 	 => '45em',
			'large-plus' => '64em',
			'original' 	 => '10em'
		);


		$image_meta = wp_get_attachment_metadata( $image_id );
		$upload_dir = wp_upload_dir();

		$image_ratio = get_image_ratio($image_meta['width'], $image_meta['height']);

		$sorted = array();
		$image_sizes = array();

		// Only if there are multiple image sizes for this image
		if ( is_array( $image_meta['sizes'] ) ) {

			uasort( $image_meta['sizes'], 'orderImagesByWidth');

			$image_upload_dir = substr($image_meta['file'], 0, strrpos( $image_meta['file'], '/' ) + 1 );
			
			foreach ( $image_meta['sizes'] as $size => $filename ) {
				if ( "thumbnail" != $size ) {
					$image_sizes[$size] = $upload_dir['baseurl'] . '/' . $image_upload_dir . $filename['file'];
				}
			}
		} 

		$image_sizes['original'] = $upload_dir['baseurl'] . '/' . $image_meta['file'];

		?>

 		<span data-picture data-width="<?php echo $image_meta['width']; ?>" data-height="<?php echo $image_meta['height']; ?>" class="figure-image">
			<?php 

			$iterator = 0;
			$image_sources = array_values( $image_sizes );

			if ( count( $image_sizes ) > 1 ) : ?>

				<?php foreach ( $image_sizes as $size => $image_url ) {

					if ( isset( $sizes_as_ems[ $size ] ) ) {
					
						if ( "" == $sizes_as_ems[$size] ) {
							echo '<span data-aspect-ratio="' . $image_ratio . '" data-id="' . $image_id . '" data-src="' . $image_url . '"></span>'. "\n";
						} else {
							echo '<span data-aspect-ratio="' . $image_ratio . '" data-id="' . $image_id . '" data-src="' . $image_url . '" data-media="(min-width: ' . $sizes_as_ems[$size] . ')" data-size="'.$size.'"></span>' . "\n";
						}
					}

					$iterator++;
				} ?>

			<?php else : ?>
				<span data-id="<?php echo $image_id; ?>" data-src="<?php echo array_pop( $image_sizes ); ?>"></span>
			<?php endif; ?>
			<noscript><?php echo wp_get_attachment_image( $image_id, 'large' ); ?></noscript>
		</span>
		<?php bcmh_picture_caption( $image_id ); ?>
	<?php
}

/**
 * 
 * @uses bcmh_get_attachment
 */
function bcmh_get_picture_caption( $attachment_id ) {
	$attachment_information = bcmh_get_attachment( $attachment_id );

	// Reverse so description comes before caption
	$attachment_information = array_reverse( $attachment_information );
	
	foreach ( $attachment_information as $key => $value ) {
		if ( in_array( $key, array( 'description','caption', 'description' ) ) && strlen($value) ) {
			return sprintf('<figcaption class="caption h4 palm-cell">%s</figcaption>', $value );
		}
	}
}

/**
 * @uses modernshows_get_picture_caption
 */
function bcmh_picture_caption( $attachment_id ) {
	echo bcmh_get_picture_caption($attachment_id);
}

/**
 * 
 * http://wordpress.org/ideas/topic/functions-to-get-an-attachments-caption-title-alt-description
 */
function bcmh_get_attachment( $attachment_id ) {

	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}